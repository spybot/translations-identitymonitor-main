msgid ""
msgstr ""
"Language: nn\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: identitymonitor.controller.direct.rsnotificationknownbreaches
#, object-pascal-format
msgid "%d acknowledged breaches."
msgstr ""

#: identitymonitor.controller.direct.rsnotificationnewbreaches
#, object-pascal-format
msgid "%d new, unacknowledged breaches!"
msgstr ""

#: identitymonitor.scheduledtask.rsscheduledtaskdisplayname
msgid "Run Identity Monitor Breach Tests"
msgstr ""

#: identitymonitor.scheduledtask.rsscheduledtaskinfodescription
msgid "This task will check your monitored email addresses for new breaches."
msgstr ""

#: identitymonitor.ui.form.rsactionimmunization
msgid "Immunization"
msgstr ""

#: identitymonitor.ui.form.rsactionsystemscan
msgid "System Scan"
msgstr ""

#: identitymonitor.ui.form.rsstatusbuttoninitemail
msgid "Email addresses..."
msgstr ""

#: identitymonitor.ui.form.rsstatusbuttoninitlogins
msgid "Login names..."
msgstr ""

#: identitymonitor.ui.frame.account.details.rsframename
msgid "Breach Details"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitoraccountstakeaction
msgctxt ""
"identitymonitor.ui.frame.account.details.rsidentitymonitoraccountstakeaction"
msgid "Take action!"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsaddeddate
msgid "Breach identified since"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsbreachack
msgid "I have acknowledged and reacted to the breach:"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsbreachdate
msgid "Breach Date"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsdataclass
msgid "My breached data"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsdomain
msgid "Domain of breached service"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsmodifieddate
msgid "Breach information updated"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsno
msgctxt "identitymonitor.ui.frame.account.details.rsidentitymonitordetailsno"
msgid "No"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailspwncount
msgid "How many accounts where affected?"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailssource
msgid "Source of information"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsverified
msgid "Was the breach verified?"
msgstr ""

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsyes
msgctxt "identitymonitor.ui.frame.account.details.rsidentitymonitordetailsyes"
msgid "Yes"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesackheader
msgid "Acknowledged"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesackno
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitesackno"
msgid "No"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesackyes
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitesackyes"
msgid "Yes"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesdateofbreach
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitesdateofbreach"
msgid "Date of breach"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesdomain
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitesdomain"
msgid "Domain"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesinformationupdate
msgid "Information Update"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsbreachedsitessitename
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitessitename"
msgid "Service"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsbreachesforaccount
#, object-pascal-format
msgid "breaches for account %0:s"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rscategorybreachedsites
msgid "Breaches Sites"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rscategorypastes
msgid "Pastes (public listing of account)"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsframename
msgid "Account Details"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsidentitymonitoraccountsremove
msgid "Remove from monitoring"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsidentitymonitoraccountstakeaction
msgctxt ""
"identitymonitor.ui.frame.account.overview.rsidentitymonitoraccountstakeaction"
msgid "Take action!"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsidentitymonitorremovedialogconfirmation
#, object-pascal-format
msgid ""
"Are you sure you want to remove the account %0:s from further monitoring?"
msgstr ""

#: identitymonitor.ui.frame.account.overview.rsidentitymonitorremovedialogheader
msgid "Please confirm removal!"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccounts
msgid "Your Monitored Accounts"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsaddemailaddress
msgid "Add email address..."
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsaddusername
msgid "Add user name..."
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsemail
msgid "Email addresses"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsheader
msgid "accounts"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstableheaderbreaches
msgid "Breaches"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstableheaderidentity
msgid "Identity"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstableheadernewbreaches
msgid "New"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstableheaderpastes
msgid "Pastes"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstakeaction
msgctxt "identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstakeaction"
msgid "Take action!"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstestpassword
msgctxt ""
"identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstestpassword"
msgid "Test password"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsusername
msgid "Usernames"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoradddialogheaderemail
msgid "Add email account..."
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoradddialogheaderusername
msgid "Add login account..."
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoradddialogpromptemail
msgid "Please enter the email address you want to monitor:"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitoradddialogpromptusername
msgid "Please enter the user name you want to monitor:"
msgstr ""

#: identitymonitor.ui.frame.accounts.rsidentitymonitordialogisnotanemailaddress
msgid ""
"That does not look like a valid email address! Do you want to try again?"
msgstr ""

#: identitymonitor.ui.frame.breachedsites.rsbreachedsitesaccounts
msgctxt "identitymonitor.ui.frame.breachedsites.rsbreachedsitesaccounts"
msgid "Accounts"
msgstr ""

#: identitymonitor.ui.frame.breachedsites.rsbreachedsitesdateofbreach
msgctxt "identitymonitor.ui.frame.breachedsites.rsbreachedsitesdateofbreach"
msgid "Date of breach"
msgstr ""

#: identitymonitor.ui.frame.breachedsites.rsbreachedsitesdomain
msgctxt "identitymonitor.ui.frame.breachedsites.rsbreachedsitesdomain"
msgid "Domain"
msgstr ""

#: identitymonitor.ui.frame.breachedsites.rsbreachedsitessitename
msgctxt "identitymonitor.ui.frame.breachedsites.rsbreachedsitessitename"
msgid "Service"
msgstr ""

#: identitymonitor.ui.frame.breachedsites.rsidentitymonitorsitesframename
msgid "List of Known Breached Services"
msgstr ""

#: identitymonitor.ui.frame.breachedsites.rsidentitymonitorsitesknown
msgid "breached services known"
msgstr ""

#: identitymonitor.ui.frame.breachedsites.rssites
msgid "Sites"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitoractionaccounts
msgid "My Accounts"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitoractionservices
msgctxt "identitymonitor.ui.frame.main.rsidentitymonitoractionservices"
msgid "Services"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitorbreachstatistics
#, object-pascal-format
msgid "%0:d services (%1:s accounts)"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitorframename
msgid "Spybot Identity Monitor"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitorlatestbreachtext
#, object-pascal-format
msgid "%0:s (updated on %1:s)"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitorlatestbreachused
msgid "Latest breach used"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitornumberofsources
msgid "Number of sources checked"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitorstatusbreachcount
#, object-pascal-format
msgid "%d breaches found!"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitorstatusnewbreachcount
#, object-pascal-format
msgid "%d new breaches!"
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitorstatusnobreaches
msgid "No breaches found."
msgstr ""

#: identitymonitor.ui.frame.main.rsidentitymonitorwheeltitle
msgid "Identity Monitor"
msgstr ""

#: identitymonitor.ui.frame.main.rsstatusbuttonbreachcount
#, object-pascal-format
msgid "%2:d breaches in %0:d accounts."
msgstr ""

#: identitymonitor.ui.frame.main.rsstatusbuttonemailaddressstatustitle
#, object-pascal-format
msgid "%1:d email addresses"
msgstr ""

#: identitymonitor.ui.frame.main.rsstatusbuttonusernamestatustitle
#, object-pascal-format
msgid "%1:d user names"
msgstr ""

#: identitymonitor.ui.frame.takeaction.rstakenactionframecaption
msgid "Recommended next steps"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rsidentitymonitoraccountstestpassword
msgid "Test"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstableheader
msgid "Password Tester"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstabletestscount
msgid "Passwords tested"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordframe
msgid "Test a Password"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogheader
msgctxt "identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogheader"
msgid "Test password"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogprompt1
msgid "Please enter the password to test."
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogprompt2
msgid "It will not be transmitted online."
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogprompt3
msgid ""
"Instead, only part of a hash of the password will be sent, and the full hash "
"compared to the returned list."
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultincludelowercase
msgid "Include lower case!"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultincludenumber
msgid "Include numbers!"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultincludesymbols
msgid "Include symbols!"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultincludeuppercase
msgid "Include upper case!"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultinuse
msgid "In use"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultlength
msgid "Make it longer!"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultunique
msgid "Unique"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultweak
msgid "Weak"
msgstr ""

#: identitymonitor.ui.frame.testpassword.rsthistabletestcount
msgid "The tested password was used this often:"
msgstr ""

#: identitymonitor.ui.settings.rsidentitymonitorsettingsalwaysshowconnections
msgid "Always show connections"
msgstr ""

#: identitymonitor.ui.settings.rsidentitymonitorsettingsscheduledtaskcaption
msgid "Re-test for breaches at each Logon (via Scheduled Task)"
msgstr ""

#: identitymonitor.ui.settings.rsidentitymonitorsettingsshowackbreachnotifications
msgid "Show notifications for known breaches"
msgstr ""

#: identitymonitor.ui.settings.rsidentitymonitorsettingsshownewbreachnotifications
msgid "Show notifications for new breaches"
msgstr ""

#: identitymonitor.ui.settings.rsidentitymonitorsettingsupgradecheck
msgid "Check for updates on program start"
msgstr ""

#: tformspybot3identitymonitor.miaccounts.caption
msgctxt "tformspybot3identitymonitor.miaccounts.caption"
msgid "Accounts"
msgstr ""

#: tformspybot3identitymonitor.mihelp.caption
msgctxt "tformspybot3identitymonitor.mihelp.caption"
msgid "Help"
msgstr ""

#: tformspybot3identitymonitor.miservices.caption
msgctxt "tformspybot3identitymonitor.miservices.caption"
msgid "Services"
msgstr ""

#: tformspybot3identitymonitor.misettings.caption
msgctxt "tformspybot3identitymonitor.misettings.caption"
msgid "Settings"
msgstr ""

#: tformspybot3identitymonitor.miviews.caption
msgctxt "tformspybot3identitymonitor.miviews.caption"
msgid "Views"
msgstr ""
