msgid ""
msgstr ""
"PO-Revision-Date: 2024-02-28 20:57+0000\n"
"Last-Translator: DeepL <noreply-mt-deepl@weblate.org>\n"
"Language-Team: Bulgarian <http://translations.spybot.de/projects/"
"spybot-identity-monitor/spybot-identity-monitor/bg/>\n"
"Language: bg\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#: identitymonitor.controller.direct.rsnotificationknownbreaches
#, object-pascal-format
msgid "%d acknowledged breaches."
msgstr "%d признати нарушения."

#: identitymonitor.controller.direct.rsnotificationnewbreaches
#, object-pascal-format
msgid "%d new, unacknowledged breaches!"
msgstr "%d нови, непризнати нарушения!"

#: identitymonitor.scheduledtask.rsscheduledtaskdisplayname
msgid "Run Identity Monitor Breach Tests"
msgstr "Извършване на тестове за нарушение на Identity Monitor"

#: identitymonitor.scheduledtask.rsscheduledtaskinfodescription
msgid "This task will check your monitored email addresses for new breaches."
msgstr "Тази задача ще провери наблюдаваните имейл адреси за нови нарушения."

#: identitymonitor.ui.form.rsactionimmunization
msgid "Immunization"
msgstr "Имунизация"

#: identitymonitor.ui.form.rsactionsystemscan
msgid "System Scan"
msgstr "Сканиране на системата"

#: identitymonitor.ui.form.rsstatusbuttoninitemail
msgid "Email addresses..."
msgstr "Имейл адреси..."

#: identitymonitor.ui.form.rsstatusbuttoninitlogins
msgid "Login names..."
msgstr "Имена за вход..."

#: identitymonitor.ui.frame.account.details.rsframename
msgid "Breach Details"
msgstr "Подробности за нарушението"

#: identitymonitor.ui.frame.account.details.rsidentitymonitoraccountstakeaction
msgctxt ""
"identitymonitor.ui.frame.account.details.rsidentitymonitoraccountstakeaction"
msgid "Take action!"
msgstr "Действайте!"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsaddeddate
msgid "Breach identified since"
msgstr "Установено нарушение след"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsbreachack
msgid "I have acknowledged and reacted to the breach:"
msgstr "Признах нарушението и реагирах на него:"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsbreachdate
msgid "Breach Date"
msgstr "Дата на нарушението"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsdataclass
msgid "My breached data"
msgstr "Моите нарушени данни"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsdomain
msgid "Domain of breached service"
msgstr "Домейн на нарушената услуга"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsmodifieddate
msgid "Breach information updated"
msgstr "Актуализиране на информацията за нарушенията"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsno
msgctxt "identitymonitor.ui.frame.account.details.rsidentitymonitordetailsno"
msgid "No"
msgstr "Не"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailspwncount
msgid "How many accounts where affected?"
msgstr "Колко акаунта са засегнати?"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailssource
msgid "Source of information"
msgstr "Източник на информация"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsverified
msgid "Was the breach verified?"
msgstr "Проверено ли е нарушението?"

#: identitymonitor.ui.frame.account.details.rsidentitymonitordetailsyes
msgctxt "identitymonitor.ui.frame.account.details.rsidentitymonitordetailsyes"
msgid "Yes"
msgstr "Да"

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesackheader
msgid "Acknowledged"
msgstr "Потвърдено"

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesackno
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitesackno"
msgid "No"
msgstr "Не"

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesackyes
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitesackyes"
msgid "Yes"
msgstr "Да"

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesdateofbreach
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitesdateofbreach"
msgid "Date of breach"
msgstr "Дата на нарушението"

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesdomain
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitesdomain"
msgid "Domain"
msgstr "Домейн"

#: identitymonitor.ui.frame.account.overview.rsbreachedsitesinformationupdate
msgid "Information Update"
msgstr "Актуализиране на информацията"

#: identitymonitor.ui.frame.account.overview.rsbreachedsitessitename
msgctxt "identitymonitor.ui.frame.account.overview.rsbreachedsitessitename"
msgid "Service"
msgstr "Услуга"

#: identitymonitor.ui.frame.account.overview.rsbreachesforaccount
#, object-pascal-format
msgid "breaches for account %0:s"
msgstr "нарушения за сметка %0:s"

#: identitymonitor.ui.frame.account.overview.rscategorybreachedsites
msgid "Breaches Sites"
msgstr "Сайтове за нарушения"

#: identitymonitor.ui.frame.account.overview.rscategorypastes
msgid "Pastes (public listing of account)"
msgstr "Пасти (публичен списък на акаунт)"

#: identitymonitor.ui.frame.account.overview.rsframename
msgid "Account Details"
msgstr "Данни за сметката"

#: identitymonitor.ui.frame.account.overview.rsidentitymonitoraccountsremove
msgid "Remove from monitoring"
msgstr "Премахване от мониторинга"

#: identitymonitor.ui.frame.account.overview.rsidentitymonitoraccountstakeaction
msgctxt ""
"identitymonitor.ui.frame.account.overview.rsidentitymonitoraccountstakeaction"
msgid "Take action!"
msgstr "Действайте!"

#: identitymonitor.ui.frame.account.overview.rsidentitymonitorremovedialogconfirmation
#, object-pascal-format
msgid ""
"Are you sure you want to remove the account %0:s from further monitoring?"
msgstr ""
"Сигурни ли сте, че искате да премахнете акаунта %0:s от по-нататъшно "
"наблюдение?"

#: identitymonitor.ui.frame.account.overview.rsidentitymonitorremovedialogheader
msgid "Please confirm removal!"
msgstr "Моля, потвърдете премахването!"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccounts
msgid "Your Monitored Accounts"
msgstr "Вашите наблюдавани акаунти"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsaddemailaddress
msgid "Add email address..."
msgstr "Добавяне на имейл адрес..."

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsaddusername
msgid "Add user name..."
msgstr "Добавяне на потребителско име..."

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsemail
msgid "Email addresses"
msgstr "Имейл адреси"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsheader
msgid "accounts"
msgstr "сметки"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstableheaderbreaches
msgid "Breaches"
msgstr "Нарушения"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstableheaderidentity
msgid "Identity"
msgstr "Идентичност"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstableheadernewbreaches
msgid "New"
msgstr "Нов"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstableheaderpastes
msgid "Pastes"
msgstr "Пасти"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstakeaction
msgctxt "identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstakeaction"
msgid "Take action!"
msgstr "Действайте!"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstestpassword
msgctxt ""
"identitymonitor.ui.frame.accounts.rsidentitymonitoraccountstestpassword"
msgid "Test password"
msgstr "Тестова парола"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoraccountsusername
msgid "Usernames"
msgstr "Потребителски имена"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoradddialogheaderemail
msgid "Add email account..."
msgstr "Добавяне на имейл акаунт..."

#: identitymonitor.ui.frame.accounts.rsidentitymonitoradddialogheaderusername
msgid "Add login account..."
msgstr "Добавяне на акаунт за вход..."

#: identitymonitor.ui.frame.accounts.rsidentitymonitoradddialogpromptemail
msgid "Please enter the email address you want to monitor:"
msgstr "Моля, въведете имейл адреса, който искате да наблюдавате:"

#: identitymonitor.ui.frame.accounts.rsidentitymonitoradddialogpromptusername
msgid "Please enter the user name you want to monitor:"
msgstr "Моля, въведете потребителското име, което искате да наблюдавате:"

#: identitymonitor.ui.frame.accounts.rsidentitymonitordialogisnotanemailaddress
msgid ""
"That does not look like a valid email address! Do you want to try again?"
msgstr "Това не прилича на валиден имейл адрес! Искате ли да опитате отново?"

#: identitymonitor.ui.frame.breachedsites.rsbreachedsitesaccounts
msgctxt "identitymonitor.ui.frame.breachedsites.rsbreachedsitesaccounts"
msgid "Accounts"
msgstr "Сметки"

#: identitymonitor.ui.frame.breachedsites.rsbreachedsitesdateofbreach
msgctxt "identitymonitor.ui.frame.breachedsites.rsbreachedsitesdateofbreach"
msgid "Date of breach"
msgstr "Дата на нарушението"

#: identitymonitor.ui.frame.breachedsites.rsbreachedsitesdomain
msgctxt "identitymonitor.ui.frame.breachedsites.rsbreachedsitesdomain"
msgid "Domain"
msgstr "Домейн"

#: identitymonitor.ui.frame.breachedsites.rsbreachedsitessitename
msgctxt "identitymonitor.ui.frame.breachedsites.rsbreachedsitessitename"
msgid "Service"
msgstr "Услуга"

#: identitymonitor.ui.frame.breachedsites.rsidentitymonitorsitesframename
msgid "List of Known Breached Services"
msgstr "Списък на известните нарушени услуги"

#: identitymonitor.ui.frame.breachedsites.rsidentitymonitorsitesknown
msgid "breached services known"
msgstr "известни нарушени услуги"

#: identitymonitor.ui.frame.breachedsites.rssites
msgid "Sites"
msgstr "Сайтове"

#: identitymonitor.ui.frame.main.rsidentitymonitoractionaccounts
msgid "My Accounts"
msgstr "Моите акаунти"

#: identitymonitor.ui.frame.main.rsidentitymonitoractionservices
msgctxt "identitymonitor.ui.frame.main.rsidentitymonitoractionservices"
msgid "Services"
msgstr "Услуги"

#: identitymonitor.ui.frame.main.rsidentitymonitorbreachstatistics
#, object-pascal-format
msgid "%0:d services (%1:s accounts)"
msgstr "%0:d услуги (%1:s сметки)"

#: identitymonitor.ui.frame.main.rsidentitymonitorframename
msgid "Spybot Identity Monitor"
msgstr "Монитор на идентичността на Spybot"

#: identitymonitor.ui.frame.main.rsidentitymonitorlatestbreachtext
#, object-pascal-format
msgid "%0:s (updated on %1:s)"
msgstr "%0:s (актуализирано на %1:s)"

#: identitymonitor.ui.frame.main.rsidentitymonitorlatestbreachused
msgid "Latest breach used"
msgstr "Последно използвано нарушение"

#: identitymonitor.ui.frame.main.rsidentitymonitornumberofsources
msgid "Number of sources checked"
msgstr "Брой проверени източници"

#: identitymonitor.ui.frame.main.rsidentitymonitorstatusbreachcount
#, object-pascal-format
msgid "%d breaches found!"
msgstr "%d открити нарушения!"

#: identitymonitor.ui.frame.main.rsidentitymonitorstatusnewbreachcount
#, object-pascal-format
msgid "%d new breaches!"
msgstr "%d нови нарушения!"

#: identitymonitor.ui.frame.main.rsidentitymonitorstatusnobreaches
msgid "No breaches found."
msgstr "Не са открити нарушения."

#: identitymonitor.ui.frame.main.rsidentitymonitorwheeltitle
msgid "Identity Monitor"
msgstr "Монитор на самоличността"

#: identitymonitor.ui.frame.main.rsstatusbuttonbreachcount
#, object-pascal-format
msgid "%2:d breaches in %0:d accounts."
msgstr "%2:d нарушения в сметките %0:d."

#: identitymonitor.ui.frame.main.rsstatusbuttonemailaddressstatustitle
#, object-pascal-format
msgid "%1:d email addresses"
msgstr "%1:d имейл адреси"

#: identitymonitor.ui.frame.main.rsstatusbuttonusernamestatustitle
#, object-pascal-format
msgid "%1:d user names"
msgstr "%1:d потребителски имена"

#: identitymonitor.ui.frame.takeaction.rstakenactionframecaption
msgid "Recommended next steps"
msgstr "Препоръчани следващи стъпки"

#: identitymonitor.ui.frame.testpassword.rsidentitymonitoraccountstestpassword
msgid "Test"
msgstr "Тест"

#: identitymonitor.ui.frame.testpassword.rstableheader
msgid "Password Tester"
msgstr "Тестер за пароли"

#: identitymonitor.ui.frame.testpassword.rstabletestscount
msgid "Passwords tested"
msgstr "Тествани пароли"

#: identitymonitor.ui.frame.testpassword.rstestpasswordframe
msgid "Test a Password"
msgstr "Тестване на парола"

#: identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogheader
msgctxt "identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogheader"
msgid "Test password"
msgstr "Тестова парола"

#: identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogprompt1
msgid "Please enter the password to test."
msgstr "Моля, въведете паролата, за да я тествате."

#: identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogprompt2
msgid "It will not be transmitted online."
msgstr "Тя няма да бъде предавана онлайн."

#: identitymonitor.ui.frame.testpassword.rstestpasswordinputdialogprompt3
msgid ""
"Instead, only part of a hash of the password will be sent, and the full hash "
"compared to the returned list."
msgstr ""
"Вместо това ще бъде изпратена само част от хеша на паролата, а пълният хеш "
"ще бъде сравнен с върнатия списък."

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultincludelowercase
msgid "Include lower case!"
msgstr "Включете малки букви!"

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultincludenumber
msgid "Include numbers!"
msgstr "Включете числа!"

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultincludesymbols
msgid "Include symbols!"
msgstr "Включете символи!"

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultincludeuppercase
msgid "Include upper case!"
msgstr "Включете главни букви!"

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultinuse
msgid "In use"
msgstr "В употреба"

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultlength
msgid "Make it longer!"
msgstr "Направете го по-дълъг!"

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultunique
msgid "Unique"
msgstr "Уникален"

#: identitymonitor.ui.frame.testpassword.rstestpasswordresultweak
msgid "Weak"
msgstr "Слаб"

#: identitymonitor.ui.frame.testpassword.rsthistabletestcount
msgid "The tested password was used this often:"
msgstr "Тестваната парола е използвана толкова често:"

#: identitymonitor.ui.settings.rsidentitymonitorsettingsalwaysshowconnections
msgid "Always show connections"
msgstr "Винаги показвайте връзките"

#: identitymonitor.ui.settings.rsidentitymonitorsettingsscheduledtaskcaption
msgid "Re-test for breaches at each Logon (via Scheduled Task)"
msgstr ""
"Повторно тестване за нарушения при всяко влизане в системата (чрез планирана "
"задача)"

#: identitymonitor.ui.settings.rsidentitymonitorsettingsshowackbreachnotifications
msgid "Show notifications for known breaches"
msgstr "Показване на известия за известни нарушения"

#: identitymonitor.ui.settings.rsidentitymonitorsettingsshownewbreachnotifications
msgid "Show notifications for new breaches"
msgstr "Показване на известия за нови нарушения"

#: identitymonitor.ui.settings.rsidentitymonitorsettingsupgradecheck
msgid "Check for updates on program start"
msgstr "Проверка за актуализации при стартиране на програмата"

#: tformspybot3identitymonitor.miaccounts.caption
msgctxt "tformspybot3identitymonitor.miaccounts.caption"
msgid "Accounts"
msgstr "Сметки"

#: tformspybot3identitymonitor.mihelp.caption
msgctxt "tformspybot3identitymonitor.mihelp.caption"
msgid "Help"
msgstr "Помощ"

#: tformspybot3identitymonitor.miservices.caption
msgctxt "tformspybot3identitymonitor.miservices.caption"
msgid "Services"
msgstr "Услуги"

#: tformspybot3identitymonitor.misettings.caption
msgctxt "tformspybot3identitymonitor.misettings.caption"
msgid "Settings"
msgstr "Настройки"

#: tformspybot3identitymonitor.miviews.caption
msgctxt "tformspybot3identitymonitor.miviews.caption"
msgid "Views"
msgstr "Прегледи"

#~ msgid "Spybot Identity Monitor - Breach Details"
#~ msgstr "Spybot Identity Monitor - Подробности за нарушението"

#~ msgid "Spybot Identity Monitor - Account Details"
#~ msgstr "Spybot Identity Monitor - Подробности за профила"
