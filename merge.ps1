﻿$scriptDirectory = Split-Path -Parent $MyInvocation.MyCommand.Definition

$files = Get-ChildItem $scriptDirectory

foreach ($file in $files) {
    & "c:\Program Files\Git\usr\bin\msgmerge.exe" --update ($scriptDirectory + "\" + $file) ($scriptDirectory + "\Spybot3IdentityMonitor.pot")
}